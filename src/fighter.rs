use crate::damage::DamUnit;
use crate::death::DeathCallback;

// combat related properties and methods (monsters, player, NPCs)
#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Fighter {
    pub max_hp: i32,
    pub hp: i32,
    pub on_death: DeathCallback,
    pub xp: i32,
    pub defense: DamUnit,
    pub power: DamUnit,
    pub no_conf: bool, // can't be confused
    pub no_stun: bool, // can't be stunned
}

impl Fighter {
    pub const fn new(
        hp: i32,
        xp: i32,
        defense: DamUnit,
        power: DamUnit,
        no_conf: bool,
        no_stun: bool,
    ) -> Self {
        Fighter {
            max_hp: hp,
            hp,
            xp,
            on_death: DeathCallback::Monster,
            defense,
            power,
            no_conf,
            no_stun,
        }
    }
}
