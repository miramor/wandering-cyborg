extern crate lz4;

use std::fs::File;
use std::io::{Read, Write};

use tcod::colors;
use tcod::console::*;
use tcod::input::{self, Key, Mouse};
use tcod::map;

use crate::ai;
use crate::damage::DamUnit;
use crate::death::DeathCallback;
use crate::dungeon;
use crate::fighter::Fighter;
use crate::gui::*;
use crate::item;
use crate::menu;
use crate::messages::{MessageLog, Messages};
use crate::object::{self, Object, PlayerAction, PLAYER};
use crate::tables;
use crate::zone;

pub const SCREEN_WIDTH: i32 = 80;
pub const SCREEN_HEIGHT: i32 = 50;
pub const LIMIT_FPS: i32 = 20;

pub const BAR_WIDTH: i32 = 20;
pub const PANEL_HEIGHT: i32 = 7;
pub const PANEL_Y: i32 = SCREEN_HEIGHT - PANEL_HEIGHT;

pub const MSG_X: i32 = BAR_WIDTH + 2;
pub const MSG_WIDTH: i32 = SCREEN_WIDTH - BAR_WIDTH - 2;
pub const MSG_HEIGHT: usize = PANEL_HEIGHT as usize - 1;

pub const MAP_WIDTH: i32 = 80;
pub const MAP_HEIGHT: i32 = 43;

pub const LEVEL_UP_BASE: i32 = 200;
pub const LEVEL_UP_FACTOR: i32 = 150;

pub const INVENTORY_WIDTH: i32 = 60;
pub const LEVEL_SCREEN_WIDTH: i32 = 60;
pub const CHARACTER_SCREEN_WIDTH: i32 = 40;

pub const FOV_ALGO: map::FovAlgorithm = map::FovAlgorithm::Basic;
pub const FOV_LIGHT_WALLS: bool = true;
pub const TORCH_RADIUS: i32 = 10;

pub struct Tcod {
    pub root: Root,
    pub con: Offscreen,
    pub panel: Offscreen,
    pub fov: map::Map,
    pub mouse: Mouse,
}

#[derive(Serialize, Deserialize)]
pub struct Game {
    pub map: dungeon::Map,
    pub log: Messages,
    pub inventory: Vec<Object>,
    pub dungeon_level: i32,
}

pub fn new_game(tcod: &mut Tcod) -> (Vec<Object>, Game) {
    let mut player = Object::new(0, 0, '@', "player", colors::WHITE, true);
    player.alive = true;
    player.fighter = Some(Fighter {
        max_hp: 100,
        hp: 100,
        on_death: DeathCallback::Player,
        xp: 0,
        defense: DamUnit {
            crushing: 1,
            ..Default::default()
        },
        power: DamUnit {
            crushing: 4,
            ..Default::default()
        },
        no_conf: false,
        no_stun: false,
    });
    let mut objects = vec![player];

    let mut game = Game {
        // generate map (it's not drawn to the screen yet)
        map: dungeon::make_map(
            1,
            &mut objects,
            &tables::all_items(),
            &tables::all_monsters(),
        ),
        log: vec![],
        inventory: vec![],
        dungeon_level: 1,
    };

    initialize_fov(&game.map, tcod);

    // a warm welcoming message!
    game.log.add(
        "You have been exiled from your commune on pain of death. No way to go but onward, out into the caves...",
        colors::RED,
    );

    (objects, game)
}

pub fn initialize_fov(map: &dungeon::Map, tcod: &mut Tcod) {
    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            tcod.fov.set(
                x,
                y,
                !map[x as usize][y as usize].block_sight,
                !map[x as usize][y as usize].blocked,
            );
        }
    }

    tcod.con.clear();
}

pub fn play_game(objects: &mut Vec<Object>, game: &mut Game, tcod: &mut Tcod) {
    let mut previous_player_position = (-1, -1);
    let mut key = Default::default();

    while !tcod.root.window_closed() {
        let fov_recompute = previous_player_position != (objects[PLAYER].x, objects[PLAYER].y);

        match input::check_for_event(input::MOUSE | input::KEY_PRESS) {
            Some((_, input::Event::Mouse(m))) => tcod.mouse = m,
            Some((_, input::Event::Key(k))) => key = k,
            _ => key = Default::default(),
        }

        render_all(tcod, game, objects, fov_recompute);
        tcod.root.flush();

        // level up if needed
        level_up(objects, game, tcod);

        for id in 0..objects.len() {
            objects[id].clear(&mut tcod.con);
        }

        tcod.root.clear();

        previous_player_position = (objects[PLAYER].x, objects[PLAYER].y);
        let player_action = handle_keys(key, tcod, game, objects);
        if player_action == PlayerAction::Exit {
            save_game(objects, game).unwrap();
            break;
        }

        let mut t = 0;
        match player_action {
            PlayerAction::TookTime { time } => {
                t += time;
                if t < 100 {
                    return;
                }
                for id in 0..objects.len() {
                    if objects[id].ai.is_some() {
                        ai::ai_take_turn(id, game, objects, &tcod.fov);
                    }
                }
            }
            _ => (),
        }
    }
}

pub fn load_game() -> Result<(Vec<Object>, Game), Box<dyn std::error::Error>> {
    let mut json_save_state = String::new();
    let file = File::open("savegame")?;
    let mut decoder = lz4::Decoder::new(file)?;
    decoder.read_to_string(&mut json_save_state)?;
    let result = serde_json::from_str::<(Vec<Object>, Game)>(&json_save_state)?;
    Ok(result)
}

fn handle_keys(
    key: Key,
    tcod: &mut Tcod,
    game: &mut Game,
    objects: &mut Vec<Object>,
) -> PlayerAction {
    use tcod::input::KeyCode::*;
    use PlayerAction::*;

    let player_alive = objects[PLAYER].alive;
    match (key, player_alive) {
        (Key { code: NumPad5, .. }, true)
        | (
            Key {
                code: Char,
                printable: 'w',
                ..
            },
            true,
        ) => TookTime { time: 100 },
        (Key { code: Up, .. }, true) | (Key { code: NumPad8, .. }, true) => {
            object::move_attack(0, -1, game, objects)
        }

        (Key { code: Down, .. }, true) | (Key { code: NumPad2, .. }, true) => {
            object::move_attack(0, 1, game, objects)
        }

        (Key { code: Left, .. }, true) | (Key { code: NumPad4, .. }, true) => {
            object::move_attack(-1, 0, game, objects)
        }

        (Key { code: Right, .. }, true) | (Key { code: NumPad6, .. }, true) => {
            object::move_attack(1, 0, game, objects)
        }

        (Key { code: Home, .. }, true) | (Key { code: NumPad7, .. }, true) => {
            object::move_attack(-1, -1, game, objects)
        }

        (Key { code: End, .. }, true) | (Key { code: NumPad1, .. }, true) => {
            object::move_attack(-1, 1, game, objects)
        }

        (Key { code: PageUp, .. }, true) | (Key { code: NumPad9, .. }, true) => {
            object::move_attack(1, -1, game, objects)
        }

        (Key { code: PageDown, .. }, true) | (Key { code: NumPad3, .. }, true) => {
            object::move_attack(1, 1, game, objects)
        }

        (
            Key {
                code: Char,
                printable: 'g',
                ..
            },
            true,
        ) => {
            // pick up an item
            let item_id = objects
                .iter()
                .position(|object| object.pos() == objects[PLAYER].pos() && object.item.is_some());
            if let Some(item_id) = item_id {
                pick_item_up(item_id, game, objects);
            }
            DidntTakeTime
        }

        (
            Key {
                code: Char,
                printable: 'i',
                ..
            },
            true,
        ) => {
            // show the inventory; if an item is selected, use it
            let inventory_index = menu::inventory_menu(
                game,
                "Press the key next to an item to use it, or any other to cancel.\n",
                &mut tcod.root,
            );
            if let Some(inventory_index) = inventory_index {
                item::use_item(inventory_index, game, objects, tcod);
            }
            DidntTakeTime
        }

        (
            Key {
                code: Char,
                printable: 'd',
                ..
            },
            true,
        ) => {
            // show the invetory; if an item is selected, drop it
            let inventory_index = menu::inventory_menu(
                game,
                "Press the key next to an item to drop it, or any other to cancel.\n",
                &mut tcod.root,
            );
            if let Some(inventory_index) = inventory_index {
                item::drop_item(inventory_index, game, objects);
            }
            DidntTakeTime
        }

        (
            Key {
                code: Char,
                printable: 'x',
                ..
            },
            true,
        ) => {
            let inventory_index = menu::inventory_menu(
                game,
                "Press the key next to an item to examine it, or any other to cancel.\n",
                &mut tcod.root,
            );
            if let Some(inventory_index) = inventory_index {
                item::examine_item(inventory_index, game, tcod);
            }
            DidntTakeTime
        }

        (
            Key {
                code: Char,
                printable: 'n',
                ..
            },
            true,
        ) => {
            // go down the stairs if the player is on them
            let player_on_stairs = objects
                .iter()
                .any(|object| object.pos() == objects[PLAYER].pos() && object.name == "stairs");
            if player_on_stairs {
                next_level(tcod, objects, game);
            }
            DidntTakeTime
        }

        (
            Key {
                code: Char,
                printable: 'c',
                ..
            },
            true,
        ) => {
            // show character info
            let player = &objects[PLAYER];
            let level = player.level;
            let level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR;
            let power = player.power(game);
            let defense = player.defense(game);
            if let Some(fighter) = player.fighter.as_ref() {
                let msg = format!(
                    "Character information

Level: {}
Experience: {}
Experience to level up: {}

Maximum HP: {}
Attack:
 +{} piercing
 +{} crushing
 +{} heat
 +{} cold
 +{} electric
 +{} chemical
Defense: 
 +{} piercing
 +{} crushing
 +{} heat
 +{} cold
 +{} electric
 +{} chemical",
                    level,
                    fighter.xp,
                    level_up_xp,
                    fighter.max_hp,
                    power.piercing,
                    power.crushing,
                    power.heat,
                    power.cold,
                    power.electric,
                    power.chemical,
                    defense.piercing,
                    defense.crushing,
                    defense.heat,
                    defense.cold,
                    defense.electric,
                    defense.chemical,
                );
                menu::msgbox(&msg, CHARACTER_SCREEN_WIDTH, &mut tcod.root);
            }
            DidntTakeTime
        }

        (
            Key {
                code: Enter,
                alt: true,
                ..
            },
            _,
        ) => {
            let fullscreen = tcod.root.is_fullscreen();
            tcod.root.set_fullscreen(!fullscreen);
            DidntTakeTime
        }

        (Key { code: Escape, .. }, _) => Exit,

        _ => DidntTakeTime,
    }
}

// add to the player's inventory and remove from the map
fn pick_item_up(object_id: usize, game: &mut Game, objects: &mut Vec<Object>) {
    if game.inventory.len() >= 26 {
        game.log.add(
            format!(
                "Your pack is full, cannot pick up {}.",
                objects[object_id].name
            ),
            colors::RED,
        );
    } else {
        let item = objects.swap_remove(object_id);
        game.log
            .add(format!("You pick up a {}", item.name), colors::GREEN);
        game.inventory.push(item);
    }
}

fn save_game(objects: &[Object], game: &Game) -> Result<(), Box<dyn std::error::Error>> {
    let save_data = serde_json::to_string(&(objects, game))?;
    let file = File::create("savegame")?;
    let mut encoder = lz4::EncoderBuilder::new()
        .level(3)
        .build(file)?;
    encoder.write_all(save_data.as_bytes())?;
    encoder.finish();
    Ok(())
}

// Advance to the next level
fn next_level(tcod: &mut Tcod, objects: &mut Vec<Object>, game: &mut Game) {
    game.log.add(
        "You take a deep breath and descend further into the caves.",
        colors::RED,
    );
    game.dungeon_level += 1;
    game.map = dungeon::make_map(
        game.dungeon_level,
        objects,
        &tables::all_items(),
        &tables::all_monsters(),
    );
    initialize_fov(&game.map, tcod);
    // TODO find a less hackish way to implement zone transitions
    if (game.dungeon_level % 5) == 1 {
        game.log
            .add(zone::current_zone(game.dungeon_level).desc, colors::WHITE);
    }
}

pub fn level_up(objects: &mut [Object], game: &mut Game, tcod: &mut Tcod) {
    let player = &mut objects[PLAYER];
    let level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR;
    // see if player's experience is enough to level up
    if player.fighter.as_ref().map_or(0, |f| f.xp) >= level_up_xp {
        // it is! level up!
        player.level += 1;
        game.log.add(
            format!(
                "Your survival skills grow stronger! \
                 You reached level {}!",
                player.level
            ),
            colors::YELLOW,
        );

        let fighter = player.fighter.as_mut().unwrap();
        let mut choice = None;
        while choice.is_none() {
            // keep asking until a choice is made
            choice = menu::menu(
                "Level up! Choose a stat to raise:\n",
                &[
                    format!(
                        "Strength (+1 crushing attack, from {})",
                        fighter.power.crushing
                    ),
                    format!(
                        "Agility (+1 piercing and +1 crushing defense, from {})",
                        fighter.defense.crushing
                    ),
                ],
                LEVEL_SCREEN_WIDTH,
                &mut tcod.root,
            );
        }
        fighter.xp -= level_up_xp;
        match choice.unwrap() {
            0 => {
                fighter.power = fighter.power.add(DamUnit {
                    crushing: 1,
                    ..Default::default()
                });
            }
            1 => {
                fighter.defense = fighter.defense.add(DamUnit {
                    piercing: 1,
                    crushing: 1,
                    ..Default::default()
                });
            }
            _ => unreachable!(),
        }
    }
}
