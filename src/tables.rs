use tcod::colors;

use crate::ai::Ai;
use crate::damage::DamUnit;
use crate::item::{Equipment, Item, Slot};
use crate::object::Object;

pub fn all_monsters() -> Vec<Object> {
    vec![
        // Just a very hungry doggo
        Object::monster(
            'C',
            "cave jackal",
            colors::YELLOW,
            1,
            20,
            35,
            DamUnit::empty(),
            DamUnit::new(4, 0, 0, 0, 0, 0),
            Ai::Tracking { range: 5 },
            false,
            false,
        ),
        // It eats trash, and it thinks you're trash
        Object::monster(
            'q',
            "feral trash bear",
            colors::DARKER_GREEN,
            2,
            30,
            100,
            DamUnit::new(2, 2, 2, 6, 0, 0),
            DamUnit::new(4, 4, 0, 0, 0, 0),
            Ai::Basic,
            false,
            false,
        ),
        // Some ghosts - vulnerable to fire and lightning, otherwise very tough
        Object::monster(
            'G',
            "traveler shade",
            colors::WHITE,
            4,
            30,
            150,
            DamUnit::new(20, 20, 0, 20, 0, 20),
            DamUnit::new(1, 1, 0, 6, 0, 0),
            Ai::Basic,
            true,
            true,
        ),
        Object::monster(
            'G',
            "wanderer shade",
            colors::LIGHT_BLUE,
            10,
            50,
            250,
            DamUnit::new(30, 30, 0, 30, 0, 30),
            DamUnit::new(2, 4, 0, 14, 0, 0),
            Ai::Tracking { range: 5 },
            true,
            true,
        ),
        Object::monster(
            'G',
            "mercenary shade",
            colors::RED,
            15,
            100,
            550,
            DamUnit::new(50, 50, 5, 0, 5, 50),
            DamUnit::new(8, 4, 14, 14, 0, 0),
            Ai::Basic,
            true,
            true,
        ),
    ]
}

pub fn all_items() -> Vec<Object> {
    vec![
        Object::item('+', "repair kit", colors::GREEN, 3, Item::RepairKit, None),
        Object::item(
            '*',
            "missile bot",
            colors::LIGHT_BLUE,
            7,
            Item::MissileBot,
            None,
        ),
        Object::item('*', "grenade", colors::ORANGE, 4, Item::Grenade, None),
        Object::item('*', "smoke bomb", colors::WHITE, 2, Item::SmokeBomb, None),
        Object::item(
            '|',
            "pocket knife",
            colors::SKY,
            1,
            Item::Equipment,
            Some(Equipment::new(
                Slot::MainHand,
                0,
                DamUnit::new(1, 1, 0, 0, 0, 0),
                DamUnit::new(3, 0, 0, 0, 0, 0),
            )),
        ),
        Object::item(
            '|',
            "hunting knife",
            colors::LIGHT_GREY,
            4,
            Item::Equipment,
            Some(Equipment::new(
                Slot::MainHand,
                0,
                DamUnit::new(1, 1, 0, 0, 0, 0),
                DamUnit::new(6, 0, 0, 0, 0, 0),
            )),
        ),
        Object::item(
            '\\',
            "crowbar",
            colors::DARK_GREY,
            3,
            Item::Equipment,
            Some(Equipment::new(
                Slot::MainHand,
                0,
                DamUnit::new(2, 3, 0, 0, 0, 0),
                DamUnit::new(0, 8, 0, 0, 0, 0),
            )),
        ),
    ]
}
