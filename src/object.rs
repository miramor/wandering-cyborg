use tcod::colors;
use tcod::console::*;
use tcod::map;
use tcod::Color;

use crate::ai::Ai;
use crate::damage::DamUnit;
use crate::dungeon;
use crate::fighter::Fighter;
use crate::game::{Game, Tcod};
use crate::item::{Equipment, Item};
use crate::messages::{MessageLog, Messages};

// player will always be the first object
pub const PLAYER: usize = 0;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Object {
    pub x: i32,
    pub y: i32,
    pub char: char,
    pub color: Color,
    pub name: String,
    pub blocks: bool,
    pub alive: bool,
    pub fighter: Option<Fighter>,
    pub ai: Option<Ai>,
    pub item: Option<Item>,
    pub always_visible: bool,
    pub level: i32,
    pub equipment: Option<Equipment>,
}

impl Object {
    pub fn new(x: i32, y: i32, char: char, name: &str, color: Color, blocks: bool) -> Self {
        Object {
            x,
            y,
            char,
            color,
            name: name.into(),
            blocks,
            alive: false,
            fighter: None,
            ai: None,
            item: None,
            always_visible: false,
            level: 1,
            equipment: None,
        }
    }

    pub fn monster(
        char: char,
        name: &str,
        color: Color,
        level: i32,
        hp: i32,
        xp: i32,
        defense: DamUnit,
        power: DamUnit,
        ai: Ai,
        no_conf: bool,
        no_stun: bool,
    ) -> Self {
        let mut mon = Object::new(0, 0, char, name, color, true);
        mon.level = level;
        mon.alive = true;
        mon.fighter = Some(Fighter::new(hp, xp, defense, power, no_conf, no_stun));
        mon.ai = Some(ai);

        mon
    }

    pub fn item(
        char: char,
        name: &str,
        color: Color,
        level: i32,
        item: Item,
        equipment: Option<Equipment>,
    ) -> Self {
        let mut it = Object::new(0, 0, char, name, color, false);
        it.level = level;
        it.always_visible = true;
        it.item = Some(item);
        it.equipment = equipment;

        it
    }

    // Set the the color, then draw the character represting this object at its position
    pub fn draw(&self, con: &mut dyn Console) {
        con.set_default_foreground(self.color);
        con.put_char(self.x, self.y, self.char, BackgroundFlag::None);
    }

    // Erase the character representing this object
    pub fn clear(&self, con: &mut dyn Console) {
        con.put_char(self.x, self.y, ' ', BackgroundFlag::None);
    }

    pub const fn pos(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn set_pos(&mut self, x: i32, y: i32) {
        self.x = x;
        self.y = y;
    }

    fn distance_grid(g1: (i32, i32), g2: (i32, i32)) -> f32 {
        (((g2.0 - g1.0).pow(2) + (g2.1 - g1.1).pow(2)) as f32).sqrt()
    }

    pub fn distance_to(&self, other: &Object) -> f32 {
        Self::distance_grid((self.x, self.y), (other.x, other.y))
    }

    pub fn distance(&self, x: i32, y: i32) -> f32 {
        Self::distance_grid((self.x, self.y), (x, y))
    }

    pub fn take_damage(&mut self, attack: DamUnit, game: &mut Game) -> Option<i32> {
        // apply damage if possible
        if let Some(fighter) = self.fighter.as_mut() {
            let damage = attack.subtract(fighter.defense).sum();
            if damage > 0 {
                game.log.add(
                    format!("{} takes {} damage.", self.name, damage),
                    colors::RED,
                );
                fighter.hp -= damage;
            } else {
                game.log
                    .add(format!("{} is unaffected.", self.name), colors::PURPLE);
            }
        }

        // check for death, call the death function
        if let Some(fighter) = self.fighter {
            if fighter.hp <= 0 {
                self.alive = false;
                fighter.on_death.callback(self, game);
                return Some(fighter.xp);
            }
        }

        None
    }

    pub fn attack(&mut self, target: &mut Object, game: &mut Game) {
        game.log.add(
            format!("{} attacks {}.", self.name, target.name),
            colors::WHITE,
        );

        if let Some(xp) = target.take_damage(self.power(game), game) {
            self.fighter.as_mut().unwrap().xp += xp;
        }
    }

    pub fn heal(&mut self, amount: i32, game: &Game) {
        let max_hp = self.max_hp(game);
        if let Some(ref mut fighter) = self.fighter {
            fighter.hp += amount;
            if fighter.hp > max_hp {
                fighter.hp = max_hp;
            }
        }
    }

    pub fn equip(&mut self, log: &mut Messages) {
        if let Some(ref mut equipment) = self.equipment {
            if !equipment.equipped {
                equipment.equipped = true;
                log.add(
                    format!("Equipped {} on {}.", self.name, equipment.slot),
                    colors::LIGHT_GREEN,
                );
            }
        }
    }

    pub fn dequip(&mut self, log: &mut Messages) {
        if let Some(ref mut equipment) = self.equipment {
            if equipment.equipped {
                equipment.equipped = false;
                log.add(
                    format!("Dequipped {} from {}.", self.name, equipment.slot),
                    colors::YELLOW,
                );
            }
        }
    }

    pub fn power(&self, game: &Game) -> DamUnit {
        let mut power = self.fighter.unwrap().power;
        for e in self.get_all_equipped(game).iter() {
            power = power.add(e.power);
        }
        power
    }

    pub fn defense(&self, game: &Game) -> DamUnit {
        let mut defense = self.fighter.unwrap().defense;
        for e in self.get_all_equipped(game).iter() {
            defense = defense.add(e.defense);
        }
        defense
    }

    pub fn max_hp(&self, game: &Game) -> i32 {
        let base_max_hp = self.fighter.map_or(0, |f| f.max_hp);
        let bonus: i32 = self
            .get_all_equipped(game)
            .iter()
            .map(|e| e.max_hp_bonus)
            .sum();
        base_max_hp + bonus
    }

    // returns a list of equipped items
    pub fn get_all_equipped(&self, game: &Game) -> Vec<Equipment> {
        if self.name == "player" {
            // TODO replace this crude hack
            game.inventory
                .iter()
                .filter(|item| item.equipment.map_or(false, |e| e.equipped))
                .map(|item| item.equipment.unwrap())
                .collect()
        } else {
            vec![] // other objects have no equipment
        }
    }
}

pub fn is_blocked(x: i32, y: i32, map: &dungeon::Map, objects: &[Object]) -> bool {
    if map[x as usize][y as usize].blocked {
        return true;
    }
    objects.iter().any(|object| {
        // true if a blocking object is present
        object.blocks && object.pos() == (x, y)
    })
}

pub fn move_by(id: usize, dx: i32, dy: i32, game: &Game, objects: &mut [Object]) -> bool {
    let (x, y) = objects[id].pos();
    let mut success = false;
    if !is_blocked(x + dx, y + dy, &game.map, objects) {
        objects[id].set_pos(x + dx, y + dy);
        success = true;
    }

    success
}

pub fn move_attack(dx: i32, dy: i32, game: &mut Game, objects: &mut [Object]) -> PlayerAction {
    use PlayerAction::*;

    // the coords the player is moving to or attacking
    let x = objects[PLAYER].x + dx;
    let y = objects[PLAYER].y + dy;

    // try to find an attackable object there
    let target_id = objects
        .iter()
        .position(|object| object.fighter.is_some() && object.pos() == (x, y));

    // attack if target found, move otherwise (unless blocked)
    match target_id {
        Some(target_id) => {
            let (player, target) = mut_two(PLAYER, target_id, objects);
            player.attack(target, game);
            if Some(target.fighter) != None {
                TookTime { time: 100 }
            } else {
                DidntTakeTime
            }
        }
        None => {
            if !is_blocked(x, y, &game.map, objects) {
                move_by(PLAYER, dx, dy, game, objects);
                TookTime { time: 100 }
            } else {
                DidntTakeTime
            }
        }
    }
}

pub fn move_towards_astar(
    id: usize,
    dest_x: i32,
    dest_y: i32,
    game: &Game,
    objects: &mut [Object],
    fov_map: &map::Map,
) {
    use tcod::pathfinding::AStar;

    let mut astar = AStar::new_from_map(fov_map.clone(), 1.0); // TODO see if this use of clone() can be elided
    if astar.find((objects[id].x, objects[id].y), (dest_x, dest_y)) {
        let step = astar.walk_one_step(false);
        match step {
            Some(step) => {
                move_by(
                    id,
                    step.0 - objects[id].x,
                    step.1 - objects[id].y,
                    game,
                    objects,
                );
            }
            None => (),
        }
    }
}

pub fn move_towards_player_astar(
    id: usize,
    game: &Game,
    objects: &mut [Object],
    fov_map: &map::Map,
) {
    move_towards_astar(
        id,
        objects[PLAYER].x,
        objects[PLAYER].y,
        game,
        objects,
        fov_map,
    );
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PlayerAction {
    TookTime { time: i32 },
    DidntTakeTime,
    Exit,
}

// Mutably borrows two *separate* elements from the given slice.
// Panics when the indexes are equal or out of bounds.
pub fn mut_two<T>(first_index: usize, second_index: usize, items: &mut [T]) -> (&mut T, &mut T) {
    assert!(first_index != second_index);
    let split_at_index = std::cmp::max(first_index, second_index);
    let (first_slice, second_slice) = items.split_at_mut(split_at_index);
    if first_index < second_index {
        (&mut first_slice[first_index], &mut second_slice[0])
    } else {
        (&mut second_slice[0], &mut first_slice[second_index])
    }
}

pub fn closest_monster(max_range: i32, objects: &mut [Object], tcod: &Tcod) -> Option<usize> {
    let mut closest_enemy = None;
    // start with (slightly more than) max range
    let mut closest_dist = (max_range + 1) as f32;

    for (id, object) in objects.iter().enumerate() {
        if (id != PLAYER)
            && object.fighter.is_some()
            && object.ai.is_some()
            && tcod.fov.is_in_fov(object.x, object.y)
        {
            let dist = objects[PLAYER].distance_to(object);
            if dist < closest_dist {
                closest_enemy = Some(id);
                closest_dist = dist;
            }
        }
    }
    closest_enemy
}
