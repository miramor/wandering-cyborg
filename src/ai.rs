use rand::Rng;
use tcod::colors;
use tcod::map;

use crate::game::Game;
use crate::messages::MessageLog;
use crate::object::{self, Object, PLAYER};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Ai {
    Basic,
    Tracking {
        range: i32,
    },
    Confused {
        previous_ai: Box<Ai>,
        num_turns: i32,
    },
}

pub fn ai_take_turn(
    monster_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    fov_map: &map::Map,
) {
    use Ai::*;
    if let Some(ai) = objects[monster_id].ai.take() {
        let new_ai = match ai {
            Basic => ai_basic(monster_id, game, objects, fov_map),
            Tracking { range } => ai_tracking(monster_id, game, objects, fov_map, range),
            Confused {
                previous_ai,
                num_turns,
            } => ai_confused(monster_id, game, objects, previous_ai, num_turns),
        };
        objects[monster_id].ai = Some(new_ai);
    }
}

fn ai_basic(monster_id: usize, game: &mut Game, objects: &mut [Object], fov_map: &map::Map) -> Ai {
    // A basic monster takes its turn. If you can see it, it can see you.
    let (monster_x, monster_y) = objects[monster_id].pos();
    if fov_map.is_in_fov(monster_x, monster_y) {
        let xp_bonus = std::cmp::max(objects[monster_id].fighter.unwrap().xp / 3, 1);
        objects[PLAYER].fighter.as_mut().unwrap().xp += xp_bonus;

        if objects[monster_id].distance_to(&objects[PLAYER]) >= 2.0 {
            // move towards player if far away
            object::move_towards_player_astar(monster_id, game, objects, fov_map);
        } else if objects[PLAYER].fighter.map_or(false, |f| f.hp > 0) {
            // close enough, attack! (if player is still alive)
            let (monster, player) = object::mut_two(monster_id, PLAYER, objects);
            monster.attack(player, game);
        }
    }
    Ai::Basic
}

fn ai_tracking(
    monster_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    fov_map: &map::Map,
    range: i32,
) -> Ai {
    // Tracking monsters follow the player even from outside LOS.
    // fov_map is still needed because the player only gains experience in LOS. :)
    let (monster_x, monster_y) = objects[monster_id].pos();
    let distance_to_player = objects[monster_id].distance_to(&objects[PLAYER]);

    if fov_map.is_in_fov(monster_x, monster_y) {
        let xp_bonus = std::cmp::max(objects[monster_id].fighter.unwrap().xp / 3, 1);
        objects[PLAYER].fighter.as_mut().unwrap().xp += xp_bonus;

        if distance_to_player < 2.0 && objects[PLAYER].fighter.map_or(false, |f| f.hp > 0) {
            let (monster, player) = object::mut_two(monster_id, PLAYER, objects);
            monster.attack(player, game);
        } else {
            object::move_towards_player_astar(monster_id, game, objects, fov_map);
        }
    } else if distance_to_player >= 2.0 && distance_to_player <= (range as f32) {
        object::move_towards_player_astar(monster_id, game, objects, fov_map);
    }

    Ai::Tracking { range }
}

fn ai_confused(
    monster_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    previous_ai: Box<Ai>,
    num_turns: i32,
) -> Ai {
    if num_turns >= 0 {
        // still confused
        // move in a random direction, decrease the number of turns remaining
        object::move_by(
            monster_id,
            rand::thread_rng().gen_range(-1, 2),
            rand::thread_rng().gen_range(-1, 2),
            game,
            objects,
        );
        Ai::Confused {
            previous_ai,
            num_turns: num_turns - 1,
        }
    } else {
        game.log.add(
            format!("The {} is no longer confused.", objects[monster_id].name),
            colors::RED,
        );
        *previous_ai
    }
}
