use tcod::colors;

use crate::game::Game;
use crate::messages::MessageLog;
use crate::object::Object;

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum DeathCallback {
    Player,
    Monster,
}

impl DeathCallback {
    pub fn callback(self, object: &mut Object, game: &mut Game) {
        use DeathCallback::*;
        let callback: fn(&mut Object, &mut Game) = match self {
            Player => player_death,
            Monster => monster_death,
        };
        callback(object, game);
    }
}

fn player_death(player: &mut Object, game: &mut Game) {
    // the game ended!
    game.log.add("You died!", colors::RED);

    // for added effect, transform the player into a corpse!
    player.char = '%';
    player.color = colors::DARK_RED;
}

fn monster_death(monster: &mut Object, game: &mut Game) {
    game.log
        .add(format!("{} dies.", monster.name), colors::ORANGE);
    *monster = Object::new(
        monster.x,
        monster.y,
        '%',
        &format!("{} corpse", monster.name),
        colors::DARK_RED,
        false,
    );
}
