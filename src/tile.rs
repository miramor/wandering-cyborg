use tcod::Color;

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Tile {
    pub blocked: bool,
    pub explored: bool,
    pub block_sight: bool,
    pub color_dark: Color,
    pub color_light: Color,
}

impl Tile {
    pub const fn empty(dark: Color, light: Color) -> Self {
        Tile {
            blocked: false,
            explored: false,
            block_sight: false,
            color_dark: dark,
            color_light: light,
        }
    }

    pub const fn wall(dark: Color, light: Color) -> Self {
        Tile {
            blocked: true,
            explored: false,
            block_sight: true,
            color_dark: dark,
            color_light: light,
        }
    }
}
