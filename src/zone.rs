use tcod::colors;
use tcod::Color;

use crate::tile::Tile;

pub struct Zone<'a> {
    pub name: &'a str,
    pub desc: &'a str,
    pub wall_type: Tile,
    pub floor_type: Tile,
    pub limits: ZoneLimits,
}

pub struct ZoneLimits {
    pub room_max_size: i32,
    pub room_min_size: i32,
    pub max_rooms: i32,
    pub room_monsters: i32,
    pub room_items: i32,
}

impl Zone<'_> {
    pub const fn new(
        name: &'static str,
        desc: &'static str,
        wall_dark: Color,
        wall_light: Color,
        floor_dark: Color,
        floor_light: Color,
        room_max_size: i32,
        room_min_size: i32,
        max_rooms: i32,
        room_monsters: i32,
        room_items: i32,
    ) -> Self {
        Zone {
            name,
            desc,
            wall_type: Tile::wall(wall_dark, wall_light),
            floor_type: Tile::empty(floor_dark, floor_light),
            limits: ZoneLimits {
                room_max_size,
                room_min_size,
                max_rooms,
                room_monsters,
                room_items,
            },
        }
    }
}

pub fn current_zone(level: i32) -> Zone<'static> {
    match level {
        1..=5 => Zone::new(
            "Junkyard",
            "This place looks like a dump. Literally.",
            colors::DARKEST_AMBER, colors::DARKER_AMBER,
            colors::DARKEST_GREY, colors::DARKER_GREY,
            10, 6, 30, 3, 2,
        ),
        6..=10 => Zone::new(
            "Sinkhole Swamp",
            "You've discovered a giant sinkhole! Weak gray light filters down from far above, and thick dark vines form walls around you.",
            colors::DARKER_GREY, colors::DARKEST_GREEN,
            colors::DARKER_GREY, colors::DARKEST_AMBER,
            8, 4, 40, 4, 1,
        ),
        11..=15 => Zone::new(
            "Underground Mansion",
            "This must have once been the home of a very powerful person, but the marble floors are now dusty and full of cracks.",
            colors::GREY, colors::WHITE,
            colors::GREY, colors::DARKEST_CRIMSON,
            12, 3, 20, 6, 4,
        ),
        _ => unreachable!(),
    }
}
