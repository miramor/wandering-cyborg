#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct DamUnit {
    pub piercing: i32,
    pub crushing: i32,
    pub heat: i32,
    pub cold: i32,
    pub electric: i32,
    pub chemical: i32,
}

impl DamUnit {
    pub const fn new(
        piercing: i32,
        crushing: i32,
        heat: i32,
        cold: i32,
        electric: i32,
        chemical: i32,
    ) -> Self {
        DamUnit {
            piercing,
            crushing,
            heat,
            cold,
            electric,
            chemical,
        }
    }

    pub const fn empty() -> Self {
        DamUnit {
            piercing: 0,
            crushing: 0,
            heat: 0,
            cold: 0,
            electric: 0,
            chemical: 0,
        }
    }

    pub fn sum(&self) -> i32 {
        let piercing = self.piercing.max(0);
        let crushing = self.crushing.max(0);
        let heat = self.heat.max(0);
        let cold = self.cold.max(0);
        let electric = self.electric.max(0);
        let chemical = self.chemical.max(0);
        piercing + crushing + heat + cold + electric + chemical
    }

    pub const fn add(&self, other: Self) -> Self {
        DamUnit {
            piercing: self.piercing + other.piercing,
            crushing: self.crushing + other.crushing,
            heat: self.heat + other.heat,
            cold: self.cold + other.cold,
            electric: self.electric + other.electric,
            chemical: self.chemical + other.chemical,
        }
    }

    pub const fn subtract(&self, other: Self) -> Self {
        DamUnit {
            piercing: self.piercing - other.piercing,
            crushing: self.crushing - other.crushing,
            heat: self.heat - other.heat,
            cold: self.cold - other.cold,
            electric: self.electric - other.electric,
            chemical: self.chemical - other.chemical,
        }
    }
}
