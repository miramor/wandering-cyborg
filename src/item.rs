use tcod::colors;

use crate::damage::DamUnit;
use crate::game::{Game, Tcod};
use crate::gui;
use crate::menu;
use crate::messages::MessageLog;
use crate::object::{self, Object, PLAYER};

const EXAMINE_SCREEN_WIDTH: i32 = 40;

const REPAIR_AMOUNT: i32 = 40;
const MISSILE_DAMAGE: i32 = 40;
const MISSILE_RANGE: i32 = 5;
const SMOKE_NUM_TURNS: i32 = 10;
const SMOKE_RANGE: i32 = 8;
const GRENADE_DAMAGE: i32 = 25;
const GRENADE_RADIUS: i32 = 3;

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum Item {
    RepairKit,
    MissileBot,
    SmokeBomb,
    Grenade,
    Equipment,
}

pub enum UseResult {
    UsedUp,
    UsedAndKept,
    Canceled,
}

pub fn use_item(inventory_id: usize, game: &mut Game, objects: &mut [Object], tcod: &mut Tcod) {
    use Item::*;
    // just call the "use_function" if it is defined
    if let Some(item) = game.inventory[inventory_id].item {
        let on_use: fn(usize, game: &mut Game, &mut [Object], &mut Tcod) -> UseResult = match item {
            RepairKit => use_repair,
            MissileBot => use_missile,
            SmokeBomb => use_smokebomb,
            Grenade => use_grenade,
            Equipment => toggle_equipment,
        };
        match on_use(inventory_id, game, objects, tcod) {
            UseResult::UsedUp => {
                // destroyed after use, unless it was canceled for some reason
                game.inventory.remove(inventory_id);
            }
            UseResult::UsedAndKept => {} // do nothing
            UseResult::Canceled => {
                game.log.add("Canceled", colors::WHITE);
            }
        }
    } else {
        game.log.add(
            format!("The {} cannot be used.", game.inventory[inventory_id].name),
            colors::WHITE,
        );
    }
}

fn use_repair(
    _inventory_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    _tcod: &mut Tcod,
) -> UseResult {
    // heal the player
    if let Some(fighter) = objects[PLAYER].fighter {
        if fighter.hp == objects[PLAYER].max_hp(game) {
            game.log
                .add("You are already fully functioning.", colors::RED);
            return UseResult::Canceled;
        }
        game.log.add(
            "You systems start to repair themselves.",
            colors::LIGHT_VIOLET,
        );
        objects[PLAYER].heal(REPAIR_AMOUNT, game);
        return UseResult::UsedUp;
    }
    UseResult::Canceled
}

fn use_missile(
    _inventory_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    tcod: &mut Tcod,
) -> UseResult {
    // find closest enemy (inside a max range) and damage it
    let monster_id = object::closest_monster(MISSILE_RANGE, objects, tcod);
    if let Some(monster_id) = monster_id {
        // zap it!
        game.log.add(
            "The missile bot zooms out of your hand and explodes!".to_string(),
            colors::LIGHT_BLUE,
        );
        let dam_unit = DamUnit {
            crushing: MISSILE_DAMAGE / 2,
            heat: MISSILE_DAMAGE / 2,
            ..Default::default()
        };
        if let Some(xp) = objects[monster_id].take_damage(dam_unit, game) {
            objects[PLAYER].fighter.as_mut().unwrap().xp += xp;
        }
        UseResult::UsedUp
    } else {
        // no enemy within max range
        game.log
            .add("No enemy is close enough to hit.", colors::RED);
        UseResult::Canceled
    }
}

fn use_smokebomb(
    _inventory_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    tcod: &mut Tcod,
) -> UseResult {
    use crate::ai::Ai;

    // ask the player for a target to confuse
    game.log.add(
        "Left click and enemy, or right click to cancel.",
        colors::LIGHT_CYAN,
    );
    let monster_id = gui::target_monster(tcod, game, objects, Some(SMOKE_RANGE as f32));
    if let Some(monster_id) = monster_id {
        if !objects[monster_id].fighter.unwrap().no_conf {
            let old_ai = objects[monster_id].ai.take().unwrap_or(Ai::Basic);
            // replace the monster's AI with a "confused" one
            // after some turns it will restore the old AI
            objects[monster_id].ai = Some(Ai::Confused {
                previous_ai: Box::new(old_ai),
                num_turns: SMOKE_NUM_TURNS,
            });
        }
        game.log.add(
            format!(
                "The {} is enveloped in chemical fog.",
                objects[monster_id].name
            ),
            colors::LIGHT_GREEN,
        );
        UseResult::UsedUp
    } else {
        // no enemy found within max range
        game.log
            .add("No enemy is close enough to strike.", colors::RED);
        UseResult::Canceled
    }
}

fn use_grenade(
    _inventory_id: usize,
    game: &mut Game,
    objects: &mut [Object],
    tcod: &mut Tcod,
) -> UseResult {
    let mut xp_to_gain = 0;
    // ask the player for a target tile to throw the fireball at
    game.log.add(
        "Left click a target tile, or right click to cancel.",
        colors::LIGHT_CYAN,
    );
    let (x, y) = match gui::target_tile(tcod, game, objects, None) {
        Some(tile_pos) => tile_pos,
        None => return UseResult::Canceled,
    };
    game.log.add("The grenade explodes!", colors::ORANGE);

    for (id, obj) in objects.iter_mut().enumerate() {
        if obj.distance(x, y) <= GRENADE_RADIUS as f32 && obj.fighter.is_some() {
            game.log
                .add(format!("The {} gets burned!", obj.name), colors::ORANGE);
            let dam_unit = DamUnit {
                piercing: GRENADE_DAMAGE / 2,
                heat: GRENADE_DAMAGE / 2,
                ..Default::default()
            };
            if let Some(xp) = obj.take_damage(dam_unit, game) {
                if id != PLAYER {
                    xp_to_gain += xp;
                }
            }
        }
    }
    objects[PLAYER].fighter.as_mut().unwrap().xp += xp_to_gain;
    UseResult::UsedUp
}

pub fn drop_item(inventory_id: usize, game: &mut Game, objects: &mut Vec<Object>) {
    let mut item = game.inventory.remove(inventory_id);
    item.set_pos(objects[PLAYER].x, objects[PLAYER].y);
    game.log
        .add(format!("You drop a {}.", item.name), colors::YELLOW);
    objects.push(item);
}

pub fn examine_item(inventory_id: usize, game: &Game, tcod: &mut Tcod) {
    let item = &game.inventory[inventory_id];
    let item_use = match item.item.unwrap() {
        Item::RepairKit => "Repair kit",
        Item::MissileBot => "Missile bot",
        Item::SmokeBomb => "Smoke bomb",
        Item::Grenade => "Grenade",
        Item::Equipment => "Equipment",
    };
    let mut msg = format!(
        "Item information

Level:  {}
Item use: {}",
        item.level, item_use,
    );
    if let Some(equipment) = item.equipment.as_ref() {
        let slot = match equipment.slot {
            Slot::OffHand => "Off hand",
            Slot::MainHand => "Fighting hand",
            Slot::Torso => "Torso",
            Slot::Hands => "Hands",
            Slot::Feet => "Feet",
            Slot::Head => "Head",
            Slot::SensorPort => "Sensor port",
            Slot::AuxPort => "Auxiliary port",
            Slot::DataPort => "Data port",
        };
        let msg_eq = format!(
            "
Slot: {}
HP bonus: {}
Attack:
 +{} piercing
 +{} crushing
 +{} heat
 +{} cold
 +{} electric
 +{} chemical
Defense:
 +{} piercing
 +{} crushing
 +{} heat
 +{} cold
 +{} electric
 +{} chemical",
            slot,
            equipment.max_hp_bonus,
            equipment.power.piercing,
            equipment.power.crushing,
            equipment.power.heat,
            equipment.power.cold,
            equipment.power.electric,
            equipment.power.chemical,
            equipment.defense.piercing,
            equipment.defense.crushing,
            equipment.defense.heat,
            equipment.defense.cold,
            equipment.defense.electric,
            equipment.defense.chemical,
        );
        msg = format!("{}{}", msg, msg_eq);
    }
    menu::msgbox(&msg, EXAMINE_SCREEN_WIDTH, &mut tcod.root);
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
// an object that can be equipped, granting bonuses.
pub struct Equipment {
    pub slot: Slot,
    pub equipped: bool,
    pub max_hp_bonus: i32,
    pub defense: DamUnit,
    pub power: DamUnit,
}

impl Equipment {
    pub const fn new(slot: Slot, hp: i32, defense: DamUnit, power: DamUnit) -> Self {
        Equipment {
            slot,
            equipped: false,
            max_hp_bonus: hp,
            defense,
            power,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum Slot {
    OffHand,
    MainHand,
    Torso,
    Hands,
    Feet,
    Head,
    SensorPort, // for sensory devices
    AuxPort,    // for misc augments
    DataPort,   // for learning modules
}

impl std::fmt::Display for Slot {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Slot::OffHand => write!(f, "off hand"),
            Slot::MainHand => write!(f, "fighting hand"),
            Slot::Torso => write!(f, "torso"),
            Slot::Hands => write!(f, "hands"),
            Slot::Feet => write!(f, "feet"),
            Slot::Head => write!(f, "head"),
            Slot::SensorPort => write!(f, "sensor port"),
            Slot::AuxPort => write!(f, "auxilliary port"),
            Slot::DataPort => write!(f, "data port"),
        }
    }
}

pub fn toggle_equipment(
    inventory_id: usize,
    game: &mut Game,
    _objects: &mut [Object],
    _tcod: &mut Tcod,
) -> UseResult {
    match game.inventory[inventory_id].equipment {
        None => UseResult::Canceled,
        Some(equipment) => {
            let slot = equipment.slot;
            if let Some(old_equipment) = get_equipped_in_slot(slot, &game.inventory) {
                game.inventory[old_equipment].dequip(&mut game.log);
            }
            game.inventory[inventory_id].equip(&mut game.log);
            UseResult::UsedAndKept
        }
    }
}

pub fn get_equipped_in_slot(slot: Slot, inventory: &[Object]) -> Option<usize> {
    for (inventory_id, item) in inventory.iter().enumerate() {
        if item
            .equipment
            .as_ref()
            .map_or(false, |e| e.equipped && e.slot == slot)
        {
            return Some(inventory_id);
        }
    }
    None
}
