use tcod::colors;
use tcod::console::*;

use crate::game;

pub fn msgbox(text: &str, width: i32, root: &mut Root) {
    let options: &[&str] = &[];
    menu(text, options, width, root);
}

pub fn menu<T: AsRef<str>>(
    header: &str,
    options: &[T],
    width: i32,
    root: &mut Root,
) -> Option<usize> {
    assert!(
        options.len() <= 26,
        "Cannot have a menu with more than 26 options."
    );

    // calculate total height for the header (after auto-wrap) and
    // one line per option
    let header_height = if header.is_empty() {
        0
    } else {
        root.get_height_rect(0, 0, width, game::SCREEN_HEIGHT, header)
    };
    let height = options.len() as i32 + header_height;

    // create an off-screen console that represents the menu's window
    let mut window = Offscreen::new(width, height);

    // print the header, with auto-wrap
    window.set_default_foreground(colors::WHITE);
    window.print_rect_ex(
        0,
        0,
        width,
        height,
        BackgroundFlag::None,
        TextAlignment::Left,
        header,
    );

    // print all the options
    for (index, option_text) in options.iter().enumerate() {
        let menu_letter = (b'a' + index as u8) as char;
        let text = format!("({}) {}", menu_letter, option_text.as_ref());
        window.print_ex(
            0,
            header_height + index as i32,
            BackgroundFlag::None,
            TextAlignment::Left,
            text,
        );
    }

    // blit the contents of the "window" to the root console
    let x = game::SCREEN_WIDTH / 2 - width / 2;
    let y = game::SCREEN_HEIGHT / 2 - height / 2;
    tcod::console::blit(&window, (0, 0), (width, height), root, (x, y), 1.0, 0.7);

    // present the root console to the player and wait for a key-press
    root.flush();
    let key = root.wait_for_keypress(true);

    // convert the ASCII code to an index
    // if it corresponds to an option, return it
    if key.printable.is_alphabetic() {
        let index = key.printable.to_ascii_lowercase() as usize - 'a' as usize;
        if index < options.len() {
            Some(index)
        } else {
            None
        }
    } else {
        None
    }
}

pub fn inventory_menu(game: &game::Game, header: &str, root: &mut Root) -> Option<usize> {
    // have a menu with each inventory item as an option
    let options = if game.inventory.is_empty() {
        vec!["Inventory is empty.".into()]
    } else {
        game.inventory
            .iter()
            .map(|item| {
                // show additional info if equipped
                match item.equipment {
                    Some(equipment) if equipment.equipped => {
                        format!("{} (on {})", item.name, equipment.slot)
                    }
                    _ => item.name.clone(),
                }
            })
            .collect()
    };

    let inventory_index = menu(header, &options, game::INVENTORY_WIDTH, root);

    // if an item was chosen, return it
    if !game.inventory.is_empty() {
        inventory_index
    } else {
        None
    }
}

pub fn main_menu(tcod: &mut game::Tcod) {
    while !tcod.root.window_closed() {
        tcod.root.set_default_foreground(colors::LIGHT_YELLOW);
        tcod.root.print_ex(
            game::SCREEN_WIDTH / 2,
            game::SCREEN_HEIGHT / 2 - 4,
            BackgroundFlag::None,
            TextAlignment::Center,
            "WANDERING CYBORG",
        );
        tcod.root.print_ex(
            game::SCREEN_WIDTH / 2,
            game::SCREEN_HEIGHT - 2,
            BackgroundFlag::None,
            TextAlignment::Center,
            "by JanaKitty!",
        );

        // show options and wait for the player's choice
        let choices = &["New game", "Resume game", "Quit"];
        let choice = menu("", choices, 24, &mut tcod.root);

        match choice {
            Some(0) => {
                // new game
                let (mut objects, mut game) = game::new_game(tcod);
                game::play_game(&mut objects, &mut game, tcod);
            }
            Some(1) => {
                // load game
                match game::load_game() {
                    Ok((mut objects, mut game)) => {
                        game::initialize_fov(&game.map, tcod);
                        game::play_game(&mut objects, &mut game, tcod);
                    }
                    Err(_e) => {
                        msgbox("\nNo saved game to load.\n", 24, &mut tcod.root);
                        continue;
                    }
                }
            }
            Some(2) => {
                // quit
                break;
            }
            _ => {}
        }
    }
}
