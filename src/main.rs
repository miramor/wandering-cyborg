extern crate rand;
extern crate tcod;
#[macro_use]
extern crate serde_derive;

use tcod::console::*;
use tcod::map;

mod ai;
mod damage;
mod death;
mod dungeon;
mod fighter;
mod game;
mod gui;
mod item;
mod menu;
mod messages;
mod object;
mod rect;
mod tables;
mod tile;
mod zone;

fn main() {
    let root = Root::initializer()
        .font("dejavu12x12_gs_tc.png", FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(game::SCREEN_WIDTH, game::SCREEN_HEIGHT)
        .title("Rust/tcod roguelike tutorial")
        .init();
    tcod::system::set_fps(game::LIMIT_FPS);
    let mut tcod = game::Tcod {
        root,
        con: Offscreen::new(game::MAP_WIDTH, game::MAP_HEIGHT),
        panel: Offscreen::new(game::SCREEN_WIDTH, game::PANEL_HEIGHT),
        fov: map::Map::new(game::MAP_WIDTH, game::MAP_HEIGHT),
        mouse: Default::default(),
    };

    menu::main_menu(&mut tcod);
}
