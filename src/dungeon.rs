extern crate rand;
extern crate tcod;

use std::cmp;

use rand::seq::SliceRandom;
use rand::Rng;
use tcod::colors;

use crate::game::{MAP_HEIGHT, MAP_WIDTH};
use crate::object::Object;
use crate::rect::Rect;
use crate::tile::Tile;
use crate::zone::{self, Zone};

pub type Map = Vec<Vec<Tile>>;

pub fn make_map(
    level: i32,
    objects: &mut Vec<Object>,
    item_list: &[Object],
    monster_list: &[Object],
) -> Map {
    use crate::object::PLAYER;
    // fill map with "unblocked" tiles
    let zone = zone::current_zone(level);
    let mut map = vec![vec![zone.wall_type; MAP_HEIGHT as usize]; MAP_WIDTH as usize];

    //Player is the first element, remove everything else
    // NOTE: works only when the player is the first object!
    assert_eq!(&objects[PLAYER] as *const _, &objects[0] as *const _);
    objects.truncate(1);

    let mut rooms = vec![];

    for _ in 0..zone.limits.max_rooms {
        // random width and height
        let w =
            rand::thread_rng().gen_range(zone.limits.room_min_size, zone.limits.room_max_size + 1);
        let h =
            rand::thread_rng().gen_range(zone.limits.room_min_size, zone.limits.room_max_size + 1);
        // random position without going outside map boundaries
        let x = rand::thread_rng().gen_range(0, MAP_WIDTH - w);
        let y = rand::thread_rng().gen_range(0, MAP_HEIGHT - h);

        let new_room = Rect::new(x, y, w, h);

        // run through the rooms and see if they intersect
        let failed = rooms
            .iter()
            .any(|other_room| new_room.intersects_with(other_room));

        if !failed {
            // no intersections, so the room is valid
            create_room(new_room, &mut map, &zone);
            place_objects(new_room, &mut map, objects, level, item_list, monster_list);
            let (new_x, new_y) = new_room.center();

            if rooms.is_empty() {
                // this is the first room, player starts here
                objects[PLAYER].set_pos(new_x, new_y);
            } else {
                // connect all rooms after the first with tunnels

                // previous room's center coordinates
                let (prev_x, prev_y) = rooms[rooms.len() - 1].center();

                // randomly start with either horizontal or vertical tunnel
                if rand::random() {
                    create_h_tunnel(prev_x, new_x, prev_y, &mut map, &zone);
                    create_v_tunnel(prev_y, new_y, new_x, &mut map, &zone);
                } else {
                    create_v_tunnel(prev_y, new_y, new_x, &mut map, &zone);
                    create_h_tunnel(prev_x, new_x, prev_y, &mut map, &zone);
                }
            }

            rooms.push(new_room);
        }
    }

    let (last_room_x, last_room_y) = rooms[rooms.len() - 1].center();
    let mut stairs = Object::new(
        last_room_x,
        last_room_y,
        '>',
        "stairs",
        colors::WHITE,
        false,
    );

    stairs.always_visible = true;
    objects.push(stairs);

    map
}

fn create_room(room: Rect, map: &mut Map, zone: &Zone) {
    for x in (room.x1 + 1)..room.x2 {
        for y in (room.y1 + 1)..room.y2 {
            map[x as usize][y as usize] = zone.floor_type;
        }
    }
}

fn create_h_tunnel(x1: i32, x2: i32, y: i32, map: &mut Map, zone: &Zone) {
    for x in cmp::min(x1, x2)..(cmp::max(x1, x2) + 1) {
        map[x as usize][y as usize] = zone.floor_type;
    }
}

fn create_v_tunnel(y1: i32, y2: i32, x: i32, map: &mut Map, zone: &Zone) {
    for y in cmp::min(y1, y2)..(cmp::max(y1, y2) + 1) {
        map[x as usize][y as usize] = zone.floor_type;
    }
}

fn place_random_object(
    x: i32,
    y: i32,
    level: i32,
    objects: &mut Vec<Object>,
    object_list: &[Object],
) {
    let mut obj: Object;
    loop {
        let o = object_list.choose(&mut rand::thread_rng());
        match o {
            Some(o) => {
                if o.level <= level {
                    obj = o.clone();
                    break;
                } else {
                    continue;
                }
            }
            None => continue,
        }
    }
    obj.x = x;
    obj.y = y;
    objects.push(obj);
}

fn place_objects(
    room: Rect,
    map: &mut Map,
    objects: &mut Vec<Object>,
    level: i32,
    item_list: &[Object],
    monster_list: &[Object],
) {
    use crate::object::is_blocked;

    let zone = zone::current_zone(level);
    let num_monsters = rand::thread_rng().gen_range(0, zone.limits.room_monsters + 1);

    for _ in 0..num_monsters {
        let x = rand::thread_rng().gen_range(room.x1 + 1, room.x2);
        let y = rand::thread_rng().gen_range(room.y1 + 1, room.y2);

        if !is_blocked(x, y, &map, objects) {
            place_random_object(x, y, level, objects, monster_list);
        }
    }

    // choose a random number of items
    let num_items = rand::thread_rng().gen_range(0, zone.limits.room_items + 1);

    for _ in 0..num_items {
        // choose a random spot for this item
        let x = rand::thread_rng().gen_range(room.x1 + 1, room.x2);
        let y = rand::thread_rng().gen_range(room.y1 + 1, room.y2);

        // only place it if the tile is not blocked
        if !is_blocked(x, y, &map, objects) {
            place_random_object(x, y, level, objects, item_list);
        }
    }
}
